﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace aras.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "User name must not be empty.")]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password must not be empty.")]
        public string password { get; set; }

        public string ReturnUrl { get; set; }
    }
}
