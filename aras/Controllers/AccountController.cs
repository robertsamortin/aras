﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using aras.Models.IRepo;
using aras.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace aras.Controllers
{
    public class AccountController : Controller
    {
        private iRegistrationRepo _repo;
        private iAccountRepository _repoAccount;
        private iRegistrationRepo _repoReg;
        private iUserManager _userManager;


        public AccountController(iRegistrationRepo repo, iAccountRepository repoAccount, iUserManager userManager,
            iRegistrationRepo repoReg)
        {
            _repo = repo;
            _repoAccount = repoAccount;
            _userManager = userManager;
            _repoReg = repoReg;

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel lv, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "User name/password not found");
                return View(lv);
            }

            var system_users = _userManager.Validate(lv.username);

            if (system_users != null)
            {
                var model = _repoAccount.login(lv.username);
                var hashedPassword = model.user_hash;
                bool validPassword = PasswordHash.ValidatePassword(lv.password, ":1000:" + model.user_salt + ":" + model.user_hash);

                if (validPassword)
                {
                     _userManager.SignIn(this.HttpContext, system_users, false);
                    
                    
                    var d = HttpContext.User.Identity.IsAuthenticated;
                    if (string.IsNullOrEmpty(lv.ReturnUrl) || lv.ReturnUrl == "/")
                        return RedirectToAction("Index", "Home");
                    
                    return View(lv.ReturnUrl);

                    
                }
            }

            ModelState.AddModelError("Error", "User name/password not found");
            return View(lv);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            _userManager.SignOut(this.HttpContext);
            return this.RedirectToAction("Login", "Account");
        }

    }
}