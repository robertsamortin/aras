﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace aras.Models
{
    public class DbCon : IdentityDbContext<IdentityUser>
    {
        public DbCon(DbContextOptions<DbCon> options) : base(options)
        {

        }
        public DbSet<system_users> system_users { get; set; }
        //public DbSet<chapters> chapters { get; set; }
        //public DbSet<members> members { get; set; }
    }
}
