﻿using aras.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aras.Models.Repo
{
    public class RegistrationRepo : iRegistrationRepo
    {
        private readonly DbCon _db;
        public RegistrationRepo(DbCon db)
        {
            _db = db;
        }
        public void add(system_users r)
        {
            _db.Add(r);
            _db.SaveChanges();
        }

        //public bool checkExistID(string idNumber)
        //{
        //    var model = _db.members.Where(e => e.idNumber == idNumber);
        //    if (model.Count() > 0)
        //        return true;
        //    return false;
        //}

        //public bool checkAlreadyRegistered(string idNumber)
        //{
        //    var model = _db.registration.Where(e => e.idNumber == idNumber);
        //    if (model.Count() > 0)
        //        return true;
        //    return false;
        //}
    }
}
