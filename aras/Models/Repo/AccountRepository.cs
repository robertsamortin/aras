﻿using aras.Models.IRepo;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Linq;

namespace aras.Models.Repo
{
    public class AccountRepository : iAccountRepository
    {
        private readonly DbCon _db;
        public AccountRepository(DbCon db)
        {
            _db = db;
        }
        
        public system_users login(string username)
        {
            var model = _db.system_users.Where(r => r.users_name  == username).FirstOrDefault();
            return model; 
        }
    }
}
