﻿using aras.Models.IRepo;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace aras.Models.Repo
{
    public class UserManager : DbCon, iUserManager
    {
        iAccountRepository _user;
        public UserManager(iAccountRepository system_users, DbContextOptions<DbCon> options) : base(options)
        {
            _user = system_users;
        }

        public system_users GetCurrentUser(HttpContext httpContext)
        {
            string currentUserId = this.GetCurrentUserId(httpContext).ToString();

            if (currentUserId == null)
                return null;
            int currUser = int.Parse(currentUserId);
            return system_users.Find(currUser);
        }

        public string GetCurrentUserId(HttpContext httpContext)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
                return null;

            Claim claim = httpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (claim == null)
                return null;

            string currentUserId = claim.Value;

            //if (!int.TryParse(claim.Value, out currentUserId))
            //    return -1;

            return currentUserId;
        }

        public async void SignIn(HttpContext httpContext, system_users system_users, bool isPersistent = false)
        {
            ClaimsIdentity identity = new ClaimsIdentity(this.GetUserClaims(system_users), CookieAuthenticationDefaults.AuthenticationScheme, "user","role");
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await httpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties() { IsPersistent = isPersistent }
            );
            httpContext.User = principal;
        }

        public async void SignOut(HttpContext httpContext)
        {
            await httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        //public registration userDetails(HttpContext httpContext)
        //{
        //    var idNumber = GetCurrentUser(httpContext).idNumber;
        //    var system_users = _user.login(idNumber);
        //    return system_users;

        //}

        public system_users Validate(string username)
        {
            var system_users = _user.login(username);

            return system_users;
        }

        private IEnumerable<Claim> GetUserClaims(system_users system_users)
        {
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, system_users.emp_code.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, system_users.access_level.ToString()));
            return claims;
        }
    }
}
