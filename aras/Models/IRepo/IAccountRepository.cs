﻿using aras.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aras.Models.IRepo
{
    public interface iAccountRepository
    {
        system_users login(string empCode);
       
    }
}
