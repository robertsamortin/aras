﻿using aras.Models;
using Microsoft.AspNetCore.Http;

namespace aras.Models.IRepo
{
    public interface iUserManager
    {
        system_users Validate(string username);
        void SignIn(HttpContext httpContext, system_users system_users, bool isPersistent = false);
        void SignOut(HttpContext httpContext);
        string GetCurrentUserId(HttpContext httpContext);
        system_users GetCurrentUser(HttpContext httpContext);
       // registration userDetails(HttpContext httpContext);
    }
}
