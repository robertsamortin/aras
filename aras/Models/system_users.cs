﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace aras.Models
{
    public class system_users
    {
        [Key]
        public int user_id { get; set; }
        public string emp_code { get; set; }
        public string users_name { get; set; }
        public string user_hash { get; set; }
        public string user_salt { get; set; }
        public int access_level { get; set; }
    }
}
